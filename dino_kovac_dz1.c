#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#define HEADER_LINE_MAX 100
#define COMMENT_CHAR '#'
#define RGB_NORMALIZED_MAX 255
#define BLOCK_SIZE 8
#define PI_OVER_8 0.3926990817
#define DCT_CONST 0.176777

const unsigned short quant_table_y[8][8] = { { 16, 11, 10, 16, 24, 40, 51, 61 },
{ 12, 12, 14, 19, 26, 58, 60, 55 },
{ 14, 13, 16, 24, 40, 57, 69, 56 },
{ 14, 17, 22, 29, 51, 87, 80, 62 },
{ 18, 22, 37, 56, 68, 109, 103, 77 },
{ 24, 35, 55, 64, 81, 104, 113, 92 },
{ 49, 64, 78, 87, 103, 121, 120, 101 },
{ 72, 92, 95, 98, 112, 100, 103, 99 } };

const unsigned short quant_table_c[8][8] = { { 17, 18, 24, 47, 99, 99, 99, 99 },
{ 18, 21, 26, 66, 99, 99, 99, 99 },
{ 24, 26, 56, 99, 99, 99, 99, 99 },
{ 47, 66, 99, 99, 99, 99, 99, 99 },
{ 99, 99, 99, 99, 99, 99, 99, 99 },
{ 99, 99, 99, 99, 99, 99, 99, 99 },
{ 99, 99, 99, 99, 99, 99, 99, 99 },
{ 99, 99, 99, 99, 99, 99, 99, 99 } };

typedef struct {
    char y;
    char cb;
    char cr;
} pixel;

typedef struct {
    unsigned int width;
    unsigned int height;
    unsigned int maxVal;
    unsigned int block;
    pixel **pixels;
} image_data;

const char *get_filename_ext(const char *filename) {
    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return dot + 1;
}

int get_line(FILE *fp, char *line) {
    do{
        char *retVal = fgets(line, HEADER_LINE_MAX, fp);
        if(retVal == NULL) {
            printf ("Error reading line from file! Aborting.\n");
            fclose(fp);
            exit(-5);
        }
    } while(line[0] == COMMENT_CHAR);

    char *comment_char = strchr(line, COMMENT_CHAR);
    if(comment_char != NULL) {
        *comment_char = '\0';
    }

    if(line[strlen(line) - 1] == '\n') {
        line[strlen(line) - 1] = '\0';
    }

    return 0;
}

void check_malloc(void *ptr) {
    if(ptr == NULL) {
        printf("Memory allocation failed! Aborting.\n");
        exit(-11);
    }
}

unsigned short normalize_rgb(int value, int maxValue) {
    int factor = 1;
    if(maxValue > RGB_NORMALIZED_MAX) {
        factor = (maxValue + 1) / (RGB_NORMALIZED_MAX + 1);
        return round((float)value / factor);
    } else if(maxValue < RGB_NORMALIZED_MAX) {
        factor = (RGB_NORMALIZED_MAX + 1) / (maxValue + 1);
        return round((float)value * factor);
    } else {
        return value;
    }
}

short rgb_2_y(unsigned short R, unsigned short G, unsigned short B) {
    return 0.299*R + 0.587*G + 0.114*B;
}

short rgb_2_cb(unsigned short R, unsigned short G, unsigned short B) {
    return -0.1687*R - 0.3313*G + 0.5*B + 128;
}

short rgb_2_cr(unsigned short R, unsigned short G, unsigned short B) {
    return 0.5*R - 0.4187*G - 0.0813*B + 128;
}

void parse_ascii_image_data(FILE *fp, image_data *data) {

    unsigned int read, i, j;
    unsigned int value_r, value_g, value_b;
    unsigned short  norm_r, norm_g, norm_b;

    for (i = 0; i < data->height; ++i)
    {
        for (j = 0; j < data->width; ++j)
        {
            read = fscanf(fp, "%d %d %d", &value_r, &value_g, &value_b);
            if(read != 3) {
                printf("Error reading image data! Aborting.\n");
                fclose(fp);
                exit(-6);
            }
            norm_r = normalize_rgb(value_r, data->maxVal);
            norm_g = normalize_rgb(value_g, data->maxVal);
            norm_b = normalize_rgb(value_b, data->maxVal);

            (data->pixels)[i][j].y = rgb_2_y(norm_r, norm_g, norm_b);
            (data->pixels)[i][j].cb = rgb_2_cb(norm_r, norm_g, norm_b);
            (data->pixels)[i][j].cr = rgb_2_cr(norm_r, norm_g, norm_b);
        }
    }
}

void parse_byte_image_data(FILE *fp, image_data *data) {

    unsigned char *rgb = (unsigned char *)malloc(3*sizeof(char));
    unsigned short  norm_r, norm_g, norm_b;
    unsigned int read, i, j;

    for (i = 0; i < data->height; ++i)
    {
        for (j = 0; j < data->width; ++j)
        {
            read = fread(rgb, 1, 3, fp);
            if(read != 3) {
                printf("Error reading image data! Aborting.\n");
                fclose(fp);
                exit(-6);
            }
            norm_r = normalize_rgb(rgb[0], data->maxVal);
            norm_g = normalize_rgb(rgb[1], data->maxVal);
            norm_b = normalize_rgb(rgb[2], data->maxVal);

            (data->pixels)[i][j].y = rgb_2_y(norm_r, norm_g, norm_b);
            (data->pixels)[i][j].cb = rgb_2_cb(norm_r, norm_g, norm_b);
            (data->pixels)[i][j].cr = rgb_2_cr(norm_r, norm_g, norm_b);
        }
    }
}

void read_ppm(char *filename, image_data *data) {

    FILE *fp;
    fp = fopen(filename, "rb");
    if(fp == NULL) {
        printf("Error openning file \"%s\"! Aborting.\n", filename);
        exit(-4);
    }

    /* read ppm header */
    char magicNumber[2];
    unsigned int width, height, maxVal;

    char *line = (char *)calloc(HEADER_LINE_MAX, sizeof(char));
    check_malloc(line);

    get_line(fp, line);

    /* try reading all header params from first line */
    unsigned int read, i;

    read = sscanf(line, "%s %d %d %d", magicNumber, &width, &height, &maxVal);

    read_header_params:
    switch(read) {
        case 1:
        get_line(fp, line);
        read = sscanf(line, "%d %d %d", &width, &height, &maxVal);
        if(read != 0)
            read += 1;
        goto read_header_params;
        break;
        case 2:
        get_line(fp, line);
        read = sscanf(line, "%d %d", &height, &maxVal);
        if(read != 0)
            read += 2;
        goto read_header_params;
        case 3:
        get_line(fp, line);
        read = sscanf(line, "%d", &maxVal);
        if(read != 1) {
            goto header_error;
        }
        break;
        case 4:
        break;
        default:
        header_error:
        printf("Error while reading ppm header! Aborting.\n");
        fclose(fp);
        exit(-5);
    }

    printf("PPM header read; magic: %s, width: %d, height: %d, maxVal: %d\n", magicNumber, width, height, maxVal);
    free(line);

    int inAscii = 0;

    if(strcmp(magicNumber, "P3") == 0) {
        inAscii = 1;
        printf("Image data is in ascii format.\n");
    } else if(strcmp(magicNumber, "P6") == 0) {
        inAscii = 0;
        printf("Image data is in byte format.\n");
        if(maxVal > 255) {
            printf ("Byte format image data supports only sigle byte colours! Aborting.\n");
            fclose(fp);
            exit(-5);
        }
    } else {
        printf("Magic number not recognized, needs to be 'P3' or 'P6' for PPM! Aborting.\n");
        fclose(fp);
        exit(-5);
    }

    if(width % BLOCK_SIZE != 0 || height % BLOCK_SIZE != 0) {
        printf("The image dimensions need to be divisible by %d! Aborting.\n", BLOCK_SIZE);
        fclose(fp);
        exit(-5);
    }

    unsigned int blockCount = (width / BLOCK_SIZE) * (height / BLOCK_SIZE);

    if(data->block >= blockCount) {
        printf("The block number needs to be smaller than the total count of blocks %d %d! Aborting.\n", data->block, blockCount);
        fclose(fp);
        exit(-5);
    }

    printf("Allocating memory for image data..\n");
    data->width = width;
    data->height = height;
    data->maxVal = maxVal;
    data->pixels = (pixel **)malloc(height * sizeof(pixel *));
    check_malloc(data->pixels);
    for (i = 0; i < height; ++i)
    {
        data->pixels[i] = (pixel *)malloc(width * sizeof(pixel));
        check_malloc(data->pixels[i]);
    }
    printf ("Memory alocated.\n");

    printf("Processing..\n");

    if(inAscii) {
        parse_ascii_image_data(fp, data);
    } else {
        parse_byte_image_data(fp, data);
    }

    fclose(fp);

}

void translate_values(image_data *data) {

    unsigned int i, j;

    for (i = 0; i < data->height; ++i)
    {
        for (j = 0; j < data->width; ++j)
        {
            (data->pixels)[i][j].y -= 128;
            (data->pixels)[i][j].cb -= 128;
            (data->pixels)[i][j].cr -= 128;
        }
    }
}

short round_dct(float f) {
    float decimal = fabs(fmod(f, 1.0f));

    if(f > 0) {
        if(decimal >= 0.5)
            return ceil(f);
        else
            return floor(f);
    } else {
        if(decimal >= 0.5)
            return floor(f);
        else
            return ceil(f);
    }
}

pixel** dct(image_data *data) {

    unsigned int i, j, u, v;
    double sum_y, sum_cb, sum_cr;

    /* find the position of the block */
    unsigned int blocks_in_row = data->width / BLOCK_SIZE;
    unsigned int start_row = (data->block / blocks_in_row) * BLOCK_SIZE;
    unsigned int start_col = (data->block % blocks_in_row) * BLOCK_SIZE;

    /* prepare input data */
    pixel **input = (pixel **)malloc(BLOCK_SIZE*sizeof(pixel *));
    check_malloc(input);
    pixel **output = (pixel **)malloc(BLOCK_SIZE*sizeof(pixel *));
    check_malloc(output);
    for( i = 0; i < BLOCK_SIZE; i++ ) {
        input[i] = (pixel *)malloc(BLOCK_SIZE*sizeof(pixel));
        output[i] = (pixel *)malloc(BLOCK_SIZE*sizeof(pixel));
        check_malloc(input[i]);
        check_malloc(output[i]);
        for( j = 0; j < BLOCK_SIZE; j++ ) {
            input[i][j] = data->pixels[start_row + i][start_col + j];
        }
    }

    for (v = 0; v < BLOCK_SIZE; v++) {
        for (u = 0; u < BLOCK_SIZE; u++) {
            sum_y = 0;
            sum_cb = 0;
            sum_cr = 0;
            for (i = 0; i < BLOCK_SIZE; i++) {
                for (j = 0; j < BLOCK_SIZE; j++) {
                    sum_y += input[i][j].y * cosf(u*(j+0.5)*PI_OVER_8) * cosf (v*(i+0.5)*PI_OVER_8);
                    sum_cb += input[i][j].cb * cosf(u*(j+0.5)*PI_OVER_8) * cosf (v*(i+0.5)*PI_OVER_8);
                    sum_cr += input[i][j].cr * cosf(u*(j+0.5)*PI_OVER_8) * cosf (v*(i+0.5)*PI_OVER_8);
                }
            }

            if (u == 0 && v == 0) {
                sum_y /= 8;
                sum_cb /= 8;
                sum_cr /= 8;
            } else if (u == 0 || v == 0) {
                sum_y *= DCT_CONST;
                sum_cb *= DCT_CONST;
                sum_cr *= DCT_CONST;
            } else {
                sum_y /= 4;
                sum_cb /= 4;
                sum_cr /= 4;
            }
            sum_y /= quant_table_y[v][u];
            sum_cb /= quant_table_c[v][u];
            sum_cr /= quant_table_c[v][u];
            output[v][u].y = round_dct(sum_y);
            output[v][u].cb = round_dct(sum_cb);
            output[v][u].cr = round_dct(sum_cr);
        }
    }

    return output;
}

void print_output(pixel **output, char *outputFile) {

    unsigned int i,j;

    FILE *fp = fopen(outputFile, "wb");
    if(fp == NULL) {
        printf("Error openning file \"%s\"! Aborting.\n", outputFile);
        exit(-4);
    }

    for( i = 0; i < BLOCK_SIZE; i++ ) {
        for( j = 0; j < BLOCK_SIZE; j++ ) {
            fprintf(fp, "%4d", output[i][j].y);
        }
        fprintf(fp, "\n");
    }
    fprintf(fp, "\n");

    for( i = 0; i < BLOCK_SIZE; i++ ) {
        for( j = 0; j < BLOCK_SIZE; j++ ) {
            fprintf(fp, "%4d", output[i][j].cb);
        }
        fprintf(fp, "\n");
    }

    fprintf(fp, "\n");

    for( i = 0; i < BLOCK_SIZE; i++ ) {
        for( j = 0; j < BLOCK_SIZE; j++ ) {
            fprintf(fp, "%4d", output[i][j].cr);
        }
        fprintf(fp, "\n");
    }

    fclose(fp);

    printf("Output data written.\nHave a nice day!\n");
}

int main(int argc, char *argv[]) {

    if(argc != 4) {
        printf("Usage: %s <pic.ppm> <block> <out.txt>\n", argv[0]);
        return -1;
    }

    char *inputFile, *blockNumber, *outputFile;
    inputFile = argv[1];
    blockNumber = argv[2];
    outputFile = argv[3];

    if(strcmp(get_filename_ext(inputFile), "ppm") != 0) {
        printf("The second arguments needs to be in ppm file format! Aborting.\n");
        return -2;
    }

    char *num;

    long block = strtol(blockNumber, &num, 0);

    if (errno) {
        printf("Error reading block number! Aborting.\n");
        return -3;
    }

    image_data *data = (image_data*)malloc(sizeof(image_data));
    check_malloc(data);
    data->block = block;

    read_ppm(inputFile, data);
    translate_values(data);

    pixel **output = dct(data);

    printf("Done.\nWriting output data..\n");

    print_output(output, outputFile);

    free(data);

    return 0;
}